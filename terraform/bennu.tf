resource "virtualbox_vm" "node" {
    count = 2
    name = "${format("vm-%02d", count.index+1)}"

    image = "~/Descargas/DevOp/xenial-server-cloudimg-amd64-vagrant.box"
    cpus = 2
    memory = "1024mib"

    network_adapter {
        type = "nat"
    }

    network_adapter {
        type = "bridged"
        host_interface = "en0"
    }
}

output "IPAddr" {
    value = "${element(virtualbox_vm.node.*.network_adapter.1.ipv4_address, 1)}"
}
