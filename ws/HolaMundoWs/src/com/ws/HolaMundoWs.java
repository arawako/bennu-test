package com.ws;

import javax.jws.*;

@WebService(serviceName = "HolaMundoService")
public class HolaMundoWs {
	
	@WebMethod
    @WebResult(name = "resultado")
    public String obtenerMensaje(){
        return "Hola Mundo!";
    }

}
